const timeConversion = function (time) {
  const timeFormat = time.split('').splice(time.length - 2, 2).join('').toLowerCase();
  let hours = time.split('').splice(0,2).join('');
  const minSec = time.split('').splice(2,time.length - 4).join('');

  if (timeFormat === 'pm' && hours < 12) hours = Number(hours) + 12;
  if (timeFormat === 'am' && hours == 12) hours = Number(hours) - 12;
  if (hours < 10) hours = "0" + Number(hours);
  return hours + minSec;
}

module.exports = timeConversion;
